# Docker tutorial code

Code used in docker tutorial presentation. Presentation and other resources can be found here: [pdf](./presentation.pdf)

The video might contain outdated information, the repository is up to date.

## Getting Started

Watch presentation video and follow the steps. This repository will serve you as a reference.

## Building and pushing the image
Check out the commands in https://gitlab.com/YOUR_USER/YOUR_REPO/container_registry

In my case

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/horosin/ardigen-kubernetes-tutorial .
docker push registry.gitlab.com/horosin/ardigen-kubernetes-tutorial
```

## Contributing

Feel free to extend this code, add examples in other languages.

## Authors

* **Karol Horosin** - *Initial work* - [horosin](https://github.com/horosin)

## Acknowledgments

* [Piotr R](https://github.com/nuada) for previous presentation
* Mateusz Siedlarz for infrastructure presentation
